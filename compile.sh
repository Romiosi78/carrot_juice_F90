dir=$PWD
cd growing
gfortran -c growing.f90
cp growing.o ../
cp growing.mod ../
cd $dir

cd pressing
gfortran -c pressing.f90
cp pressing.o ../
cp pressing.mod ../
cd $dir

gfortran pressing.o growing.o carrot_juice.f90
rm *.o
rm *.mod
